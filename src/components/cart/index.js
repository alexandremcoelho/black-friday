import "./card.css";

const Cart = ({ item, useCart, setCart }) => {
  const remove = (e) => {
    const name = e.target.getAttribute("name");
    setCart(useCart.filter((item) => item.name !== name));
  };
  return (
    <div className="itens">
      <div>{item.name}</div>
      <div>{item.price}</div>
      <button name={item.name} onClick={remove}>
        remove
      </button>
    </div>
  );
};
export default Cart;
