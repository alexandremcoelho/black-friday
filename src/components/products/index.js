import "../cart/card.css";

const Produtos = ({ item, useCart, setCart }) => {
  const add = () => {
    setCart([...useCart, item]);
  };

  return (
    <div className="itens">
      <div>{item.name}</div>
      <div>{item.price}</div>
      <button onClick={() => add()}>add</button>
    </div>
  );
};
export default Produtos;
