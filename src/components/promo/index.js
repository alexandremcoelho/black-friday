const Promo = ({ item, useCarrinho, setCarrinho, promo }) => {
  const desc = (item.price * (promo / 100)).toFixed(2);
  const newPrice = item.price - desc;

  const add = () => {
    const newItem = {
      id: item.id,
      name: item.name,
      price: Number(newPrice),
    };
    console.log(useCarrinho);
    setCarrinho([...useCarrinho, newItem]);
  };
  return (
    <div>
      <div>{item.name}</div>
      <div>{item.price}</div>
      <div>{promo}%</div>
      <div>-{desc}</div>
      <div>FINAL = {newPrice.toFixed(2)}</div>
      <button onClick={() => add()}>adicionar ao carrinho</button>
    </div>
  );
};

export default Promo;
// Nome do Produto
// Preço original;
// Porcentagem do desconto;
// Valor do desconto;
// Preço a ser pago,
// Botão para adicionar ao seu carrinho.
