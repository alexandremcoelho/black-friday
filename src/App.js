import "./App.css";
import { useState } from "react";
import Produtos from "./components/products";
import Cart from "./components/cart";
import Promo from "./components/promo";

function App() {
  const [useProduct, setProduct] = useState([
    { id: 1, name: "Smart TV LED 50", price: 1999.0 },
    { id: 2, name: "PlayStation 5", price: 12000.0 },
    { id: 3, name: "Notebook Acer Nitro 5", price: 5109.72 },
    { id: 4, name: "Headset s fio Logitech G935", price: 1359.0 },
    { id: 5, name: "Tablet Samsung Galaxy Tab S7", price: 4844.05 },
    { id: 6, name: "Cadeira Gamer Cruiser Preta FORTREK", price: 1215.16 },
  ]);
  const [desconto, setDesconto] = useState();
  const [index, setIndex] = useState();
  const reducer = (accumulator, currentValue) =>
    accumulator + currentValue.price;

  const [useCart, setCart] = useState([0]);

  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  const getNumbers = () => {
    setDesconto(getRandomIntInclusive(4, 7) * 10);
    setIndex(getRandomIntInclusive(0, 5));
  };
  const total = useCart.reduce(reducer);

  return (
    <div>
      <div>
        <div id="loja">
          <div id="title">loja</div>
          <div>
            {useProduct &&
              useProduct.map((item, index) => (
                <Produtos
                  item={item}
                  key={index}
                  useCart={useCart}
                  setCart={setCart}
                />
              ))}
          </div>
          <div id="itens">
            <button
              onClick={() => getNumbers()}
              style={{ backgroundColor: "red" }}
            >
              GERAR PROMOÇÃO
            </button>
            {desconto && (
              <Promo
                item={useProduct[index]}
                useCarrinho={useCart}
                setCarrinho={setCart}
                promo={desconto}
              />
            )}
          </div>
        </div>

        <div id="cart">
          <div id="footer">carrinho</div>

          {useCart.map((item, index) => (
            <Cart item={item} key={index} useCart={useCart} setCart={setCart} />
          ))}
          {total && <div>total ={total.toFixed(2)}</div>}
        </div>
      </div>
    </div>
  );
}

export default App;
